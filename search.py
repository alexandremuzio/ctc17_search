import queue as Q
from math import sqrt

#################
#Representations#
#################
class Node(object):
    """Graph Node"""
    def __init__(self, id, x = 0, y = 0):
        self.id = id
        self.x = x
        self.y = y

    def __str__(self):
        return "%d - (%f, %f)" % (self.id, self.x, self.y)

def dist(a, b):
    return sqrt((a.x - b.x)**2 + (a.y - b.y)**2)
        
class Graph(object):
    """Graph representation"""
    def __init__(self):
        self.adj_map = {}
        self.nodes = {}

    def __str__(self):
        res = ""
        for node,adj_li in self.adj_map.items():
            res += "%s: %s\n" %(node, adj_li)
        return res

    def add_node(self, node):
        self.adj_map[node.id] = []
        self.nodes[node.id] = node

    def add_edge(self, ori_id, dest_id):
        self.adj_map[ori_id].append(dest_id);

    def get_neighbors(self, node_id):
        return self.adj_map[node_id]

    def get_node(self, node_id):
        return self.nodes[node_id]

    def dist(self, ori, dest):
        node_ori = self.get_node(ori)
        node_dest = self.get_node(dest)
        return dist(node_ori, node_dest)


############
#Algorithms#
############
class BestFirstSearch(object):
    """Greedy search algorithm"""
    def __init__(self, graph, ori_id, dest_id):
        self.graph = graph
        self.ori = graph.get_node(ori_id)
        self.dest = graph.get_node(dest_id)

    def h(self, n_id):
        node = self.graph.get_node(n_id)
        return sqrt((node.x - self.dest.x)**2 + \
            (node.y - self.dest.y)**2)

    def run(self):
        pq = Q.PriorityQueue()
        f = {}
        parent = {}
        visited = {}

        f[self.ori.id] = 0.0
        pq.put(self.ori.id, self.h(self.ori.id))

        while not pq.empty():
            curr_node = pq.get()

            if curr_node == self.dest.id:
                path = []
                path.append(curr_node)
                parent_idx = self.dest.id
                while parent_idx != self.ori.id:
                    path.append(parent_idx)
                    parent_idx = parent[parent_idx]
                path.reverse()
                return f[self.dest.id], path

            for neigh in self.graph.get_neighbors(curr_node):
                edge_w = self.graph.dist(curr_node, neigh)
                if neigh not in f:
                    f[neigh] = f[curr_node] + edge_w
                    pq.put(neigh, self.h(neigh))
                    parent[neigh] = curr_node
        return -1.0


class Astar(object):
    """Greedy search algorithm"""
    def __init__(self, graph, ori_id, dest_id):
        self.graph = graph
        self.ori = graph.get_node(ori_id)
        self.dest = graph.get_node(dest_id)

    def h(self, n_id):
        node = self.graph.get_node(n_id)
        return sqrt((node.x - self.dest.x)**2 + \
            (node.y - self.dest.y)**2)

    def run(self):
        pq = Q.PriorityQueue()
        f = {}
        parent = {}

        f[self.ori.id] = 0.0
        pq.put(self.ori.id, self.h(self.ori.id))

        while(not pq.empty()):
            curr_node = pq.get()

            if curr_node == self.dest.id:
                path = []
                path.append(curr_node)
                parent_idx = self.dest.id
                while parent_idx != self.ori.id:
                    path.append(parent_idx)
                    parent_idx = parent[parent_idx]
                path.reverse()
                return f[self.dest.id], path

            for neigh in self.graph.get_neighbors(curr_node):
                edge_w = self.graph.dist(curr_node, neigh)
                if neigh not in f or f[curr_node] + edge_w < f[neigh]:
                    f[neigh] = f[curr_node] + edge_w
                    pq.put(neigh, self.h(neigh) + f[neigh])
                    parent[neigh] = curr_node
        return -1.0

        
def create_graph_from_file(filename):
    with open(filename) as f:
        g = Graph()
        for line in f:
            tokenized_line = line.replace(",", ".").split(';')

            node_id = int(tokenized_line[0])
            x = float(tokenized_line[1])
            y = float(tokenized_line[2])

            neigh_list = []
            for neigh in tokenized_line[3:]:
                if (neigh.isdigit()):
                    neigh_list.append(int(neigh))

            g.add_node(Node(node_id, x, y))

            for neigh in neigh_list:
                g.add_edge(node_id, neigh)
            #print (node_id, x, y, neigh_list)
        return g

def main():
    g = create_graph_from_file('Uruguay.csv')
    ori = 202
    dest = 601
    # print (g)
    bfs = BestFirstSearch(g, ori, dest)
    dist, path_bfs = bfs.run()
    print ("Dist from BFS is %f" % dist)
    print (path_bfs)

    a_star = Astar(g, ori, dest)
    dist_a_star, path_a_star = a_star.run()
    print ("Dist from Astar is %f" % dist_a_star)
    print (path_a_star)

if __name__ == '__main__':
    main()