import copy
import math

class TicTacToeState(object):
    def __init__(self, board, current_player):
        self.board = board
        self.current_player = current_player

    def get_child_states(self):
        neighbors = []
        for idx, val in enumerate(self.board):
            if val == '-':
                self.board[idx] = self.current_player
                neighbors.append((TicTacToeState(copy.deepcopy(self.board), self.get_other_player()), idx))
                self.board[idx] = '-'

        # print (self.board)
        #print ([(x.board, x.current_player, y) for x, y in neighbors])
        # input("Test")
        return neighbors

    def get_other_player(self):
        if self.current_player == 'X':
            return 'O'
        else:
            return 'X'

    def is_leaf(self):
        if self.board.count("-") == 0:
            return True
        #horizontal
        for l in range(0, 3):
            if (self.board[3*l] == self.board[3*l + 1] == self.board[3*l + 2]) and self.board[3*l] != '-':
                return True
        #vertical
        for l in range(0, 3):
            if (self.board[l] == self.board[l + 3] == self.board[l + 6]) and self.board[l] != '-':
                return True
        #diag
        if (self.board[0] == self.board[4] == self.board[8]) and self.board[0] != '-':
             return True
        if (self.board[2] == self.board[4] == self.board[6]) and self.board[2] != '-':
            return True

        return False

    def __str__(self):
        return str((self.board, self.current_player))

def evaluate_state(state, max_player):
    winner = None
    #horizontal
    for l in range(0, 3):
        st = l * 3
        if state.board[st] == state.board[st + 1] == state.board[st + 2]:
            if state.board[st] == max_player:
                return 10
            else:
                return -10

    #vertical
    for l in range(0, 3):
        if state.board[l] == state.board[l + 3] == state.board[l + 6]:
            if state.board[l] == max_player:
                return 10
            else:
                return -10

    #diag
    if state.board[0] == state.board[4] == state.board[8]:
        if state.board[0] == max_player:
            return 10
        else:
            return -10

    elif state.board[2] == state.board[4] == state.board[6]:
        if state.board[2] == max_player:
            return 10
        else:
            return -10

    return 0

def mini_max_alfabeta(node, depth, a, b, max_player):
    # print ("*************", node.board, depth,a, b, max_player, "***********")
    # print ("Node is leaf:", node.is_leaf() )
    if depth == 0 or node.is_leaf():
        return evaluate_state(node, 'O'), None

    # input("debug")

    if max_player:
        v = -99999
        for child,act in node.get_child_states():
            v_curr, _ = mini_max_alfabeta(child, depth - 1, a, b, False)
            if v_curr > v:
                v = v_curr
                action = act
            a = max(a, v_curr)
            if b <= a:
               break #branch cut-off
        return v, action
    else:
        v = 9999999
        for child,act in node.get_child_states():
            v_curr, _ = mini_max_alfabeta(child, depth - 1, a, b, True)
            if v_curr < v:
                v = v_curr
                action = act
            b = min(b, v_curr)
            if b <= a:
              break #branch cut-off
        return v, action


#####################
#Game Implementation#
#####################
class TicTacToe(object):
    """Tic-tac-toe implementation"""
    def __init__(self):
        self.board = ['-']*9
        self.finished = False
        self.winner = None

    def get_board(self):
        return self.board

    def play(self, player, action):
        self.board[action - 1] = player

        #Check board status
        print ("Finished = %s" % self.finished)
        print (self.winner)
        #horizontal
        for l in range(0, 3):
            if self.board[3*l] == self.board[3*l + 1] == self.board[3*l + 2] and self.board[3*l] != '-':
                self.winner = self.board[3*l]
                self.finished = True
        #vertical
        for l in range(0, 3):
            if self.board[l] == self.board[l + 3] == self.board[l + 6] and self.board[l] != '-':
                self.winner = self.board[l]
                self.finished = True
        #diag
        if self.board[0] == self.board[4] == self.board[8] and self.board[0] != '-':
             self.winner = self.board[0]
             self.finished = True
        if self.board[2] == self.board[4] == self.board[6] and self.board[2] != '-':
            self.winner = self.board[2]
            self.finished = True

    def possible_plays(self):
        possible_plays = [idx + 1 for idx,b in enumerate(self.board) if b == '-']
        return possible_plays


    def print_board(self):
        print ("Board:")
        print ("%s|%s|%s\n%s|%s|%s\n%s|%s|%s" % (
            self.board[0], self.board[1], self.board[2], 
            self.board[3], self.board[4], self.board[5],
            self.board[6], self.board[7], self.board[8]))
        pass

    def has_finished(self):
        return self.finished

    def get_winner(self):
        return self.winner

class Player(object):
    def play(self, game):
        raise NotImplementedError("Should have implemented this.")

class HumanPlayer(Player):
    def __init__(self, id = "X"):
        self.id = id

    def play(self, game):
        #Read from input
        human_input = "0"
        while human_input > "9" or human_input < "1" or int(human_input) not in game.possible_plays():
            human_input = input("Where to move:\n1 2 3\n4 5 6\n7 8 9\n")
        game.play(self.id, int(human_input))

class AIPlayer(Player):
    def __init__(self, id = "O"):
        self.id = id

    def play(self, game):
        v, action =  mini_max_alfabeta(TicTacToeState(game.board, self.id), 100, -math.inf, math.inf, True)
        print ("v = %s and action = %s" % (v, action))
        game.play(self.id, int(action) + 1)

############
#Game Utils#
############
def print_game_results(game):
    game.print_board()

    print ("________________")
    if (game.get_winner() == None):
        print("Draw!")
    else:
        print ("The Winner is %s" % game.get_winner())
    print("##########################################")

def print_game_start():
    print("############")
    print("TIC TAC TOE#")
    print("############")

def print_game_iteration(it, player):
    print("\n##########################################")
    print("Iteration %d" % (it))
    
    if player == 0:
        print ("YOUR TURN (X)")
    else:
        print ("COMPUTER'S TURN (O)")

    print ("________________")


#############
#Game Update#
#############
def main():
    game = TicTacToe()

    players = [HumanPlayer(), AIPlayer()]
    current_player_idx = 0

    it = 1
    print_game_start()
    while not game.has_finished():
        print_game_iteration(it, current_player_idx)
        #Player move
        players[current_player_idx].play(game)

        game.print_board()
        current_player_idx ^= 1
        it += 1


    print_game_results(game)

def test():
    board = ['O', '-', 'X',\
             '-', 'X', 'O', \
             'X', 'O', 'X']
    state_test = TicTacToeState(board, 'O')

    print(evaluate_state(state_test, 'O'))

if __name__ == "__main__":
   main()
   #test()
